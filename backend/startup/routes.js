const express = require('express');
const cors = require('cors')
const serverRunning = require('../routes/serverRunning');
const users = require('../routes/users');
const auth = require('../routes/auth');
const todo = require('../routes/todo');
const error = require('../middleware/error');

module.exports = function(app) {
  app.use(cors());
  app.use(express.json());
  app.use('/', serverRunning);
  app.use('/api/todo', todo);
  app.use('/api/users', users);
  app.use('/api/auth', auth);
  app.use(error);
}