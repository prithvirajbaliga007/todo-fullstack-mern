### Todo backend

Default PORT is set as 3100. Use can also use a .env to set a port if needed, with in case will take the PORt mentioned in .env file
This todo application has these set of apis:
1) Login - `POST /api/auth { email: '', password: '' }`
2) Registeration - `POST /api/auth { name: '', email: '', password: '' }`
3) Get user account info - `POST /api/auth/me`
4) Create a new todo item - `Header x-auth-token POST /api/users { todoItem: '' }`
5) Get todo list - `Header x-auth-token GET /api/users `
6) Get a todo item (Commented the code, can be used if required) - `Header x-auth-token GET /api/users/:id`
7) Update a todo item - `Header x-auth-token PUT /api/users/:id`
8) Delete a todo item - `Header x-auth-token DELETE /api/users/:id`

To Run the application:
1) Create a .env file at the root level of the project with todo_jwtPrivateKey=xxxxxx variable. Set value as a scret private key.
2) Run the commond `npm run start`. 
