import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import ProtectedRoute from "./component/protectedRoute";

import Header from './component/header';
import Loading from './component/loading';
import ApiMsg from './component/apiMsg';

import Login from './pages/login';
import Loader from './component/loader';
const LazyRegistration = React.lazy(() => import('./pages/registration'));
const LazyTodo = React.lazy(() => import('./pages/todo'))
const LazyNotFound = React.lazy(() => import('./pages/notFound'))


function App() {

  return (
    <>
      <ApiMsg/>
      <Loading />
      <Header />
      <div className="custom-container">
        <React.Suspense fallback={<Loader/>}>
          <BrowserRouter>
            <Switch>
                <Route path="/" component={Login} exact/>
                <Route path="/registration" component={LazyRegistration} />
                <ProtectedRoute exact path="/todo" component={LazyTodo} />
                <Route path="*" component={LazyNotFound} />
            </Switch>
          </BrowserRouter>
        </React.Suspense>
      </div>
    </>
  );
}

export default App;
