import React from 'react';

import useApiStatus from '../hooks/useApiStatus';
import Loader from './loader';

const Loading = () => {
    const [ loadingAndMsgStatus ] = useApiStatus();
    return (
      <>
        { loadingAndMsgStatus.loader && <Loader/>}
      </>
    );
  }
  
  export default Loading;
  