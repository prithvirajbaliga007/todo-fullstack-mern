import React from 'react';
import styles from '../styles/apiMsg.module.scss';
import useApiStatus from '../hooks/useApiStatus';

const ApiMsg = () => {
    const [ loadingAndMsgStatus ] = useApiStatus();
    return (
      <>
      { (loadingAndMsgStatus.message || loadingAndMsgStatus.error)  && 
        <div className={`${styles.msgWrapper} ${loadingAndMsgStatus.error ? styles.error : ''}`}>
          {loadingAndMsgStatus.error ? loadingAndMsgStatus.error : loadingAndMsgStatus.message}
        </div> 
      }   
      </>
    )
  }
  
export default ApiMsg;
  