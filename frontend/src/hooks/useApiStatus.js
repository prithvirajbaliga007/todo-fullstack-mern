import { useState, useEffect } from "react";
import { getLoaderAndMsgStatus } from '../common/helper/fetchData';

function useApiStatus() {
    const initialLoadingAndMsgStatus = { loader: false, message: '', error: '' };
    const [loadingAndMsgStatus, setLoadingAndMsgStatus] = useState(initialLoadingAndMsgStatus);
  
    useEffect(() => {
      const loader = getLoaderAndMsgStatus().subscribe((status) => {
        if(status) {
          setLoadingAndMsgStatus(status);
        } else {
          setLoadingAndMsgStatus(status);
        }
        if(status.message || status.error) {
          setTimeout(() => setLoadingAndMsgStatus(initialLoadingAndMsgStatus), 3000);
        }
      });
      return () => {
        loader.unsubscribe();
      }
    },[])

    return [loadingAndMsgStatus];
}

export default useApiStatus
